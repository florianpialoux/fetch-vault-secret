# fetch-vault-secret

Test repository used to fetch secrets from https://vault.bluelight.co/

## References
- https://about.gitlab.com/blog/2023/02/28/oidc/
- https://docs.gitlab.com/ee/ci/secrets/
- https://docs.gitlab.com/ee/ci/examples/authenticating-with-hashicorp-vault/
- https://docs.gitlab.com/ee/ci/yaml/index.html#secretsvault
- https://holdmybeersecurity.com/2021/03/04/gitlab-ci-cd-pipeline-with-vault-secrets/
- https://freedium.cfd/https://blog.devops.dev/gitlab-ci-cd-pipeline-authenticating-and-reading-vault-secrets-using-gitlab-api-5b32774e210e
- https://medium.com/@jackalus/dynamic-aws-credentials-in-gitlab-pipelines-e7d931f41ac6
